var buttonReset = document.getElementById('resetButton');
buttonReset.addEventListener('click', () => {
    handleReset(svArr);
})
var svArr = [];

const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";
// lấy dữ liệu từ local Storage
var svArrJson = localStorage.getItem('DSSV_LOCALSTORAGE');
if (svArrJson) {
    // tạo arr mới ko có hàm tính trung bình
    svArr = JSON.parse(svArrJson);
    for (var i = 0; i < svArr.length; i++) {
        sv = svArr[i];
        svArr[i] = new SinhVien(
            sv.id,
            sv.name,
            sv.email,
            sv.password,
            sv.math,
            sv.physical,
            sv.chemistry
        )
    }
    renderTable(svArr);
}


function themSV() {
    var newSV = layThongTinTuForm();
    // !validation :

    var isValid = true;
    // ! xử lý ID
    isValid = validator.kiemTraRong(newSV.id, 'spanMaSV', "ID ko dc rỗng")
        && validator.kiemTraDoDai(newSV.id, 'spanMaSV', "ID phải có độ dài là 4", 4, 4)
        && validator.kiemTraTrungID(svArr, newSV.id, 'spanMaSV', "ID bị trùng rồi bạn ạ");
    // !xử lý name
    isValid &= validator.kiemTraRong(newSV.name, 'spanTenSV', "Tên ko dc rỗng")
        && validator.kiemTraTen(newSV.name, 'spanTenSV', "Tên ko dc chứa kí tự  số");
    //! xử lý email
    isValid &=
        validator.kiemTraRong(newSV.email, 'spanEmailSV', "email ko dc rỗng") && validator.kiemTraEmail(newSV.email, 'spanEmailSV', "email ko đúng định dạng");
    //! xử lý mật khẩu
    isValid &=
        validator.kiemTraRong(newSV.password, 'spanMatKhau', 'mật khẩu ko dc rỗng')
        && validator.kiemTraMatKhau(newSV.password, 'spanMatKhau', `mật khẩu ít nhất 8 kí tự, ít nhất 1 số, 1 chữ cái thường, 1 chữ cái hoa, ko chứa kí tự đặc biệt
        `)
    //! xử lý điểm toán
    isValid &=
        validator.kiemTraRong(newSV.math, 'spanToan', 'Vui lòng nhập điểm toán')
        && validator.kiemTraDiemSo(newSV.math, 'spanToan', 'Điểm số phải từ 0 đến 10', 0, 10);
    // !xử lý điểm lý
    isValid &=
        validator.kiemTraRong(newSV.physical, 'spanLy', 'Vui lòng nhập điểm lý')
        && validator.kiemTraDiemSo(newSV.physical, 'spanLy', 'Điểm số phải từ 0 đến 10', 0, 10);
    //! xử lý điểm hóa
    isValid &= validator.kiemTraRong(newSV.chemistry, 'spanHoa', 'Vui lòng nhập điểm hóa')
        && validator.kiemTraDiemSo(newSV.chemistry, 'spanHoa', 'Điểm số phải từ 0 đến 10', 0, 10);
    if (!isValid) {
        return;
    }
    //! lưu  instance vào mảng
    svArr.push(newSV);
    //! convert thành JSON và lưu vào local storage    
    var svArrJson = JSON.stringify(svArr);
    localStorage.setItem(DSSV_LOCALSTORAGE, svArrJson);

    handleReset(svArr);
}
function handleDelete(id) {
    // tạo mảng mới-- bỏ phần tử cần lọc
    var newArr = svArr.filter((sv) => {
        if (sv.id != id) {
            return sv;
        }
    })
    // có mảng mới đã lọc

    svArr = [...newArr];

    // convert thành JSON và lưu vào local storage    
    var svArrJson = JSON.stringify(svArr);
    localStorage.setItem(DSSV_LOCALSTORAGE, svArrJson);


    renderTable(svArr);
}
function handleEdit(id) {
    var svEdit = svArr.find((sv) => {
        return sv.id == id;
    })
    showThongTinLenForm(svEdit);
    document.getElementById('txtMaSV').disabled = true;
}
function handleUpdate() {
    var newSV = layThongTinTuForm();
    document.getElementById('txtMaSV').disabled = false;
    if (!newSV.id) {
        alert("Không có gì để cập nhật");
        return;
    }
    //! validate
    var isValid = true;
    // xử lý name
    // dấu & cho phép code đi tiếp kể cả khi isValid ban đầu là false
    isValid = isValid & validator.kiemTraRong(newSV.name, 'spanTenSV', "Tên ko dc rỗng")
    // xử lý email
    isValid &=
        validator.kiemTraRong(newSV.email, 'spanEmailSV', "email ko dc rỗng") && validator.kiemTraEmail(newSV.email, 'spanEmailSV', "email ko đúng định dạng");
    // xử lý mật khẩu
    isValid &=
        validator.kiemTraRong(newSV.password, 'spanMatKhau', 'mật khẩu ko dc rỗng')
        && validator.kiemTraMatKhau(newSV.password, 'spanMatKhau', `mật khẩu ít nhất 8 kí tự, ít nhất 1 số, 1 chữ cái thường, 1 chữ cái hoa, ko chứa kí tự đặc biệt
          `)
    // xử lý điểm toán
    isValid &=
        validator.kiemTraRong(newSV.math, 'spanToan', 'Vui lòng nhập điểm toán')
        && validator.kiemTraDiemSo(newSV.math, 'spanToan', 'Điểm số phải từ 0 đến 10', 0, 10);
    // xử lý điểm lý
    isValid &=
        validator.kiemTraRong(newSV.physical, 'spanLy', 'Vui lòng nhập điểm lý')
        && validator.kiemTraDiemSo(newSV.physical, 'spanLy', 'Điểm số phải từ 0 đến 10', 0, 10);
    // xử lý điểm hóa
    isValid &=
        validator.kiemTraRong(newSV.chemistry, 'spanHoa', 'Vui lòng nhập điểm hóa')
        && validator.kiemTraDiemSo(newSV.chemistry, 'spanHoa', 'Điểm số phải từ 0 đến 10', 0, 10);

    if (!isValid) {
        return;
    }
    var hasID = false;
    for (var i = 0; i < svArr.length; i++) {
        if (svArr[i].id == newSV.id) {
            svArr[i] = newSV;
            hasID = true;
        }
    }
    if (!hasID) {
        alert("Không có ID này")
        return;
    }
    var svArrJson = JSON.stringify(svArr);
    localStorage.setItem(DSSV_LOCALSTORAGE, svArrJson);
    handleReset(svArr);
}
function handleReset(svArr) {
    document.getElementById('txtMaSV').value = '';
    document.getElementById('txtTenSV').value = '';
    document.getElementById('txtEmail').value = '';
    document.getElementById('txtPass').value = '';
    document.getElementById('txtDiemToan').value = '';
    document.getElementById('txtDiemLy').value = '';
    document.getElementById('txtDiemHoa').value = '';
    //
    document.getElementById('spanMaSV').innerText = '';
    document.getElementById('spanTenSV').innerText = '';
    document.getElementById('spanEmailSV').innerText = '';
    document.getElementById('spanMatKhau').innerText = '';
    document.getElementById('spanToan').innerText = '';
    document.getElementById('spanLy').innerText = '';
    document.getElementById('spanHoa').innerText = '';
    //
    document.getElementById('txtMaSV').disabled = false;

    renderTable(svArr);
}
function handleSearch() {
    var searchName = document.getElementById('txtSearch').value;
    var searchArr = [];
    svArr.forEach((sv) => {
        if (sv.name.includes(searchName)) {
            // console.log('sv.name: ', sv.name);
            searchArr.push(sv);
        }
    })
    renderTable(searchArr);

}
function handleOnChangeInput() {
    handleSearch();
}