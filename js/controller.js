// lưu các hàm xử lý trên giao diện
function renderTable(svArr) {
    const tbody = document.getElementById('tbodySinhVien');
    var rows = '';

    svArr.sort((a, b) => {
        if (a.id > b.id) {
            return 1;
        }
        if (a.id < b.id) {
            return -1;
        }
        return 0;
    })

    svArr.forEach((item) => {
        var row = `
        <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.email}</td>
        <td>${item.calculateAverage()}</td>
        <td>    
        <button onclick='handleEdit(${item.id})' class="btn btn-warning">Sửa</button>
        <button onclick='handleDelete(${item.id})' class="btn btn-danger">Xóa</button> 
        </td>
        </tr>
        `;
        rows += row;
    })
    tbody.innerHTML = rows;
}
function layThongTinTuForm() {
    const id = document.getElementById('txtMaSV').value;
    const name = document.getElementById('txtTenSV').value;
    const email = document.getElementById('txtEmail').value;
    const password = document.getElementById('txtPass').value;
    const math = document.getElementById('txtDiemToan').value;
    const physical = document.getElementById('txtDiemLy').value;
    const chemistry = document.getElementById('txtDiemHoa').value;
    return new SinhVien(id, name, email, password, math, physical, chemistry);
}
function showThongTinLenForm(svEdit) {

    document.getElementById('txtMaSV').value = svEdit.id;
    document.getElementById('txtTenSV').value = svEdit.name;
    document.getElementById('txtEmail').value = svEdit.email;
    document.getElementById('txtPass').value = svEdit.password;
    document.getElementById('txtDiemToan').value = svEdit.math;
    document.getElementById('txtDiemLy').value = svEdit.physical;
    document.getElementById('txtDiemHoa').value = svEdit.chemistry;
}
