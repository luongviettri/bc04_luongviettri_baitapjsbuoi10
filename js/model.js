function SinhVien(id, name, email, password, math, physical, chemistry) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.password = password;
    this.math = math;
    this.physical = physical;
    this.chemistry = chemistry;
    this.calculateAverage = () => {
        var averageScore = (+this.math + +this.physical + +this.chemistry) / 3;
        averageScore = averageScore.toFixed(2);
        return averageScore;
    }
}

